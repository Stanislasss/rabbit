# rabbit

Reactive microservices with `golang`, `rabbitmq` and `protobuf`. Read more from [here](https://medium.com/rahasak/reactive-microservices-with-golang-rabbitmq-and-protobuf-af025f4ec27).
